class CesgdbImageCapturingToImageProcessingWorkflows < ActiveRecord::Migration[5.1]
  def change
    rename_table :cesgdb_imagecapturing, :image_processing_workflows
  end
end
