class CesgdbAuftraegeToJobs < ActiveRecord::Migration[5.1]
  def change
    rename_table :cesgdb_auftraege, :jobs
  end
end
