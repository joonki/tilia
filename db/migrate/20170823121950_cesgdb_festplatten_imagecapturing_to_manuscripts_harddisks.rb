class CesgdbFestplattenImagecapturingToManuscriptsHarddisks < ActiveRecord::Migration[5.1]
  def change
    rename_table "cesgdb_festplatten-imagecapturing", :manuscripts_harddisks
  end
end
