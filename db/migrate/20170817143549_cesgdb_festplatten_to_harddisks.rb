class CesgdbFestplattenToHarddisks < ActiveRecord::Migration[5.1]
  def change
    rename_table :cesgdb_festplatten, :harddisks
  end
end
