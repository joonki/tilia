class CesgdbHssToManuscripts < ActiveRecord::Migration[5.1]
  def change
    rename_table :cesgdb_hss, :manuscripts
  end
end
