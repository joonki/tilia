class ManuscriptsHarddisksCodexIdToManuscriptsHarddisksManuscriptId < ActiveRecord::Migration[5.1]
  def change
    rename_column :manuscripts_harddisks, :codex_id, :manuscript_id
    rename_column :manuscripts_harddisks, :disk_id, :harddisk_id
  end
end
