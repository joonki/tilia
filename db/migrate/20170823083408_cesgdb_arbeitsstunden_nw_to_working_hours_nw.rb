class CesgdbArbeitsstundenNwToWorkingHoursNw < ActiveRecord::Migration[5.1]
  def change
    rename_table :cesgdb_arbeitsstunden_nw, :working_hours_nw
  end
end
