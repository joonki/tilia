
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `remember_token_expires_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cesgdb_arbeitsstunden_fmb_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cesgdb_arbeitsstunden_fmb_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(11) NOT NULL DEFAULT '0',
  `monat` int(11) NOT NULL DEFAULT '0',
  `sollstunden` float NOT NULL DEFAULT '0',
  `effektive_stunden` float NOT NULL DEFAULT '0',
  `stunden_ecodices` float DEFAULT NULL,
  `stunden_fondationbodmer` float DEFAULT NULL,
  `stunden_fondation_andere` float DEFAULT NULL,
  `ferien` float DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cesgdb_arbeitsstunden_ph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cesgdb_arbeitsstunden_ph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(11) NOT NULL DEFAULT '0',
  `monat` int(11) NOT NULL DEFAULT '0',
  `sollstunden` float NOT NULL DEFAULT '0',
  `effektive_stunden` float NOT NULL DEFAULT '0',
  `ferien` float DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cesgdb_arbeitsstunden_ub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cesgdb_arbeitsstunden_ub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(11) NOT NULL DEFAULT '0',
  `monat` int(11) NOT NULL DEFAULT '0',
  `sollstunden` float NOT NULL DEFAULT '0',
  `effektive_stunden` float NOT NULL DEFAULT '0',
  `ferien` float DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cesgdb_msdescriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cesgdb_msdescriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codex_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `bibl` text COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `encode` tinyint(4) NOT NULL,
  `bemerkung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2464 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cesgdb_msdescriptions_unimaged`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cesgdb_msdescriptions_unimaged` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kuerzel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bibliothek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `signatur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `bibl` text COLLATE utf8_unicode_ci NOT NULL,
  `encode` tinyint(4) NOT NULL,
  `bemerkung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `harddisks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harddisks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ort` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `marke` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kapazitaet` int(11) NOT NULL DEFAULT '0',
  `preis` float DEFAULT '0',
  `kaufdatum` date DEFAULT '0000-00-00',
  `letzte_kontrolle` date NOT NULL DEFAULT '0000-00-00',
  `bemerkungen` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `image_processing_workflows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_processing_workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kuerzel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ort` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bibliothek` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `datum_anfang` date NOT NULL DEFAULT '0000-00-00',
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fehlende_seiten` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'keine',
  `falsche_seiten` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `anzahl_seiten` int(11) NOT NULL DEFAULT '0',
  `speicherplatz` float NOT NULL DEFAULT '0',
  `archiviert_sg` date DEFAULT '0000-00-00',
  `disk_id` int(11) NOT NULL COMMENT 'ID der Harddisk auf der Handschrift archiviert ist',
  `thema` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `sponsor` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `online` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `kurzbeschr_de` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `kurzbeschr_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `kurzbeschr_fr` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `kurzbeschr_it` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `bemerkung` text COLLATE utf8_unicode_ci,
  `metadaten_umgesetzt` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `metadaten_aktualisieren` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `PDF_vorhanden` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nein',
  `metadaten_bibl` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `online_since` date NOT NULL,
  `fremddigitalisiert` tinyint(1) NOT NULL DEFAULT '0',
  `digitalisiert_in` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'St. Gallen' COMMENT 'Digitalisierungsort',
  `bemerkungen_statistics` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3013 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abgeschlossen` tinyint(1) DEFAULT '0',
  `auftraggeber` text CHARACTER SET utf8,
  `datum_auftrag` date DEFAULT '0000-00-00',
  `auftrags_nr` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `digitalisierungsvertrag_geschickt` date DEFAULT '0000-00-00',
  `leihvertrag_geschickt` date DEFAULT '0000-00-00',
  `digitalisierungsvertrag_zurueck` date DEFAULT '0000-00-00',
  `leihvertrag_zurueck` date DEFAULT '0000-00-00',
  `anhang_geschickt` date DEFAULT '0000-00-00',
  `anhang_zurueck` date DEFAULT '0000-00-00',
  `termin_hintransport` date DEFAULT '0000-00-00',
  `termin_ruecktransport` date DEFAULT '0000-00-00',
  `hss_angekommen` date DEFAULT '0000-00-00',
  `hss_zurueck` date DEFAULT '0000-00-00',
  `preis` float DEFAULT '0',
  `waehrung` varchar(255) CHARACTER SET utf8 DEFAULT 'CHF',
  `rechnung_geschickt` date DEFAULT '0000-00-00',
  `zahlung_erhalten` date DEFAULT '0000-00-00',
  `tiff_geschickt` date DEFAULT '0000-00-00',
  `gut_zum_druck` date DEFAULT '0000-00-00',
  `bemerkungen` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `manuscripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manuscripts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auftraege_id` int(11) DEFAULT NULL,
  `kuerzel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ort` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bibliothek` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `shelfmark` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bemerkungen` text CHARACTER SET utf8,
  `mit_aufschaltung` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=744 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `manuscripts_harddisks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manuscripts_harddisks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manuscript_id` int(11) NOT NULL DEFAULT '0',
  `harddisk_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1921 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `working_hours_Ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `working_hours_Ml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(11) NOT NULL DEFAULT '0',
  `monat` int(11) NOT NULL DEFAULT '0',
  `sollstunden` float NOT NULL DEFAULT '0',
  `effektive_stunden` float NOT NULL DEFAULT '0',
  `stunden_ecodices` float DEFAULT NULL,
  `stunden_fondationbodmer` float DEFAULT NULL,
  `stunden_fondation_andere` float DEFAULT NULL,
  `ferien` float DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `working_hours_cs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `working_hours_cs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(11) NOT NULL DEFAULT '0',
  `monat` int(11) NOT NULL DEFAULT '0',
  `sollstunden` float NOT NULL DEFAULT '0',
  `effektive_stunden` float NOT NULL DEFAULT '0',
  `ferien` float DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `working_hours_nw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `working_hours_nw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jahr` int(11) NOT NULL DEFAULT '0',
  `monat` int(11) NOT NULL DEFAULT '0',
  `sollstunden` float NOT NULL DEFAULT '0',
  `effektive_stunden` float NOT NULL DEFAULT '0',
  `stunden_ecodices` float DEFAULT NULL,
  `stunden_fondationbodmer` float DEFAULT NULL,
  `stunden_fondation_andere` float DEFAULT NULL,
  `ferien` float DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

INSERT INTO `schema_migrations` (version) VALUES
('20170814090303'),
('20170814100517'),
('20170817143549'),
('20170817151456'),
('20170823082214'),
('20170823083408'),
('20170823083447'),
('20170823121950'),
('20170823124938'),
('20170823134234');


