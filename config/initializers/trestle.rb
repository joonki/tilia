Trestle.configure do |config|
  # == Customization Options
  #
  # Set the page title shown in the main header within the admin.
  #
  config.site_title = "Tilia"

  # Specify a custom image to be used in place of the site title for mobile and
  # expanded/desktop navigation. These images should be placed within your
  # asset paths, e.g. app/assets/images.
  #
  config.site_logo = "logo.png"

  # Specify a custom image to be used for the collapsed/tablet navigation.
  #
  # config.site_logo_small = "logo-small.png"

  # Set the text shown in the page footer within the admin.
  # Defaults to 'Powered by Trestle'.
  #
  config.footer = "Tilia - made by e-codices."

  # == Mounting Options
  #
  # Set the path at which to mount the Trestle admin. Defaults to /admin.
  #
  # config.path = "/admin"

  # Toggle whether Trestle should automatically mount the admin within your
  # Rails application's routes. Defaults to true.
  #
  # config.automount = false

  # == Navigation Options
  #
  # Set the initial breadcrumbs to display in the breadcrumb trail.
  # Defaults to a breadcrumb labeled 'Home' linking to to the admin root.
  #
  # config.root_breadcrumbs = -> { [Trestle::Breadcrumb.new("Home", Trestle.config.path)] }

  # Set the default icon class to use when it is not explicitly provided.
  # Defaults to "fa fa-arrow-circle-o-right".
  #
  # config.default_navigation_icon = "fa fa-arrow-circle-o-right"

  # Add an explicit menu block to be added to the admin navigation.
  #
  # config.menu do
  #   group "Custom Group" do
  #     item "Custom Link", "/admin/custom", icon: "fa fa-car", badge: { text: "NEW!", class: "label-success" }, priority: :first
  #   end
  # end
  config.menu do

    item "Statistik", "/admin/statistics", icon: "fa fa-bar-chart"
    item "Repro", "/admin/repro", icon: "fa fa-camera-retro", priority: :first
    item "Festplatten", "/admin/harddisks", icon: "fa fa-hdd-o"

    group "Aufträge" do
      item "Aufträge", "/admin/jobs", icon: "fa fa-tasks"
      # item "Rechnungen", "/admin/invoices", icon: "fa fa-credit-card"
      item "Handschriften", "/admin/manuscripts", icon: "fa fa-book"
    end

    group "Arbeitsstunden", priority: :last do
      item "Christa Schaffert-Bosshard", "/admin/working_hours_cs", icon: "fa fa-user-o", priority: :first
      item "Maïna Loat", "/admin/working_hours_ml", icon: "fa fa-user-o", badge: { text: "NEW!", class: "label-success" }
      item "Naomi Wenger", "/admin/working_hours_nw", icon: "fa fa-user-o", badge: { text: "NEW!", class: "label-success" }
    end
  end

  # == Extension Options
  #
  # Specify a custom hook to be injected into the admin.
  #
  # config.hook(:stylesheets) do
  #   stylesheet_link_tag "custom"
  # end

  # Toggle whether Turbolinks is enabled within the admin.
  # Defaults to true if Turbolinks is available.
  #
  config.turbolinks = false

  # Specify the parameters that should persist across requests when
  # paginating or reordering. Defaults to [:sort, :order, :scope].
  #
  # config.persistent_params << :query

  # Customize the default adapter class used by all admin resources.
  # See the documentation on Trestle::Adapters::Adapter for details on
  # the adapter methods that can be customized.
  #
  # config.default_adapter.extend MyAdapterExtensions

  # Register a form field type to be made available to the Trestle form builder.
  # Field types should conform to the following method definition:
  #
  # class CustomFormField
  #   def initialize(builder, template, name, options={}, &block); end
  #   def render; end
  # end
  #
  # config.form_field :custom, CustomFormField

  # == Authentication Options
  #
  # Specify the user class to be used by trestle-auth.
  #
  config.auth.user_class = -> { Administrator }

  # Specify the scope for valid admin users.
  # Defaults to config.auth.user_class (unscoped).
  #
  # config.auth.user_scope = -> { User.where(admin: true) }

  # Specify the Trestle admin for managing administrator users.
  #
  config.auth.user_admin = -> { :"auth/administrators" }

  # Specify the parameter (along with a password) to be used to
  # authenticate an administrator. Defaults to :email.
  #
  # config.auth.authenticate_with = :login

  # Customize the method for authenticating a user given login parameters.
  # The block should return an instance of the auth user class, or nil.
  #
  # config.auth.authenticate = ->(params) {
  #   User.authenticate(params[:login], params[:password])
  # }

  # Customize the rendering of user avatars. Can be disabled by setting to false.
  # Defaults to the Gravatar based on the user's email address.
  #
  # config.auth.avatar = ->(user) {
  #   image_tag(user.avatar_url, alt: user.name)
  # }

  # Customize the method for determining the user's locale.
  # Defaults to user.locale (if the method is defined).
  #
  # config.auth.locale = ->(user) {
  #   user.locale if user.respond_to?(:locale)
  # }

  # Enable or disable remember me functionality. Defaults to true.
  #
  # config.auth.remember.enabled = false

  # Specify remember me expiration time. Defaults to 2 weeks.
  #
  config.auth.remember.for = 30.days

  # Customize the method for authenticating a user given a remember token.
  #
  # config.auth.remember.authenticate = ->(token) {
  #   User.authenticate_with_remember_token(token)
  # }

  # Customize the method for remembering a user.
  #
  # config.auth.remember.remember_me, ->(user) { user.remember_me! }

  # Customize the method for forgetting a user.
  #
  # config.auth.remember.forget_me, ->(user) { user.forget_me! }

  # Customize the method for generating the remember cookie.
  #
  # config.auth.remember.cookie, ->(user) {
  #   { value: user.remember_token, expires: user.remember_token_expires_at }
  # }
end
