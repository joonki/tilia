Trestle.resource(:harddisks) do

  search do |q|
    Harddisk.where("bezeichnung LIKE ?", "%#{q}%")
  end

  # Customize the table columns shown on the index view.
  #
  # table do
  #   column :name
  #   column :created_at, align: :center
  #   actions
  # end

  # Customize the form fields shown on the new/edit views.
  #
  # form do |harddisk|
  #   text_field :name
  #
  #   row do
  #     col(xs: 6) { datetime_field :updated_at }
  #     col(xs: 6) { datetime_field :created_at }
  #   end
  # end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  # params do |params|
  #   params.require(:harddisk).require(:name, ...)
  # end

  table do
    column :bezeichnung
    column :ort
    column :marke
    column :kapazitaet
    column :preis
    column :kaufdatum, ->(ob) { ob.kaufdatum&.strftime('%d.%m.%Y') }
    column :letzte_kontrolle, ->(ob) { ob.letzte_kontrolle&.strftime('%d.%m.%Y') }
    column :bemerkungen
  end
end
