Trestle.resource(:repro) do

  search do |q|
    ImageProcessingWorkflow.where("kuerzel LIKE ?", "%#{q}%")
  end

  table do
    column :kuerzel
    column :ort
    column :bibliothek
    column :datum_anfang, ->(ob) { ob.datum_anfang&.strftime('%d.%m.%Y') }
    column :archiviert_sg, ->(ob) { ob.archiviert_sg&.strftime('%d.%m.%Y') }
    column :fehlende_seiten
    column :falsche_seiten
    column :fremddigitalisiert
    column :digitalisiert_in
    column :bemerkung
    actions
  end
end
