Trestle.resource(:statistics) do

  search do |q|
    Statistic.where("kuerzel LIKE ?", "%#{q}%")
  end

  table do
    column :kuerzel
    column :ort
    column :bibliothek
    column :datum_anfang, ->(ob) { ob.datum_anfang&.strftime('%d.%m.%Y') }
    column :archiviert_sg, ->(ob) { ob.archiviert_sg&.strftime('%d.%m.%Y') }
    column :anzahl_seiten
    column :speicherplatz
    column :fremddigitalisiert
    column :digitalisiert_in
    column :bemerkungen_statistics
    actions
  end
end
