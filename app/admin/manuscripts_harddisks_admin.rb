Trestle.resource(:manuscripts_harddisks) do
  table do
    column :manuscript_id, ->(ob) { ob.manuscript&.kuerzel }
    column :harddisk_id, ->(ob) { ob.harddisk&.bezeichnung }
    actions
  end
end
