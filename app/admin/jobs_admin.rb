Trestle.resource(:jobs) do

  search do |q|
    Job.where("auftrags_nr LIKE ?", "%#{q}%")
  end

  table do
    column :abgeschlossen
    column :datum_auftrag, ->(ob) { ob.datum_auftrag&.strftime('%d.%m.%Y') }
    column :auftrags_nr
    column :auftraggeber
    column :preis
    column :waehrung
    column :rechnung_geschickt
    column :zahlung_erhalten
    column :bemerkungen
    actions
  end
end
