Trestle.resource(:manuscripts) do

  search do |q|
    Manuscript.where("kuerzel LIKE ?", "%#{q}%")
  end

  table do
    # column :id
    column :kuerzel
    column :ort
    column :bibliothek
    column :shelfmark
    column :bemerkungen
    column :mit_aufschaltung, align: :center do |manuscript|
      status_tag(icon("fa fa-check"), :success) if manuscript.mit_aufschaltung?
    end
    actions
  end
end
