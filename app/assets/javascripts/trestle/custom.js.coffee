# This file may be used for providing additional customizations to the Trestle
# admin. It will be automatically included within all admin pages.
#
# For organizational purposes, you may wish to define your customizations
# within individual partials and `require` them here.
#
#  e.g. #= require "trestle/custom/my_custom_js"

Trestle.ready ->
  $('input[type="date"][data-picker="true"]').flatpickr
    allowInput: true
    altInput:   true
    altFormat:  "d/m/Y"
    dateFormat:  "d/m/Y"

  $('input[type="datetime"][data-picker="true"], input[type="datetime-local"][data-picker="true"]').flatpickr
    enableTime: true
    allowInput: true
    altInput:   true
    altFormat:  "d/m/Y h:i K"
    dateFormat:  "d/m/Y h:i K"

  $('input[type="time"][data-picker="true"]').flatpickr
    enableTime: true
    noCalendar: true
    allowInput: true
    altInput:   true
    altFormat:  "h:i K"
