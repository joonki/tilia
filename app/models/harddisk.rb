class Harddisk < ApplicationRecord
  paginates_per 150

  has_many :manuscripts_harddisks
  has_many :manuscripts, through: :manuscripts_harddisks

  default_scope { order(kaufdatum: :desc) }

  # Harddisk.order('id ASC').reorder('kaufdatum DESC')
end
