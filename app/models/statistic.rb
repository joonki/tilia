class Statistic < ApplicationRecord
  paginates_per 150

  self.table_name = 'image_processing_workflows'
  default_scope { order(datum_anfang: :desc) }
end
