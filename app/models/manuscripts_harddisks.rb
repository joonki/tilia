class ManuscriptsHarddisks < ApplicationRecord
  paginates_per 150

  belongs_to :manuscript
  belongs_to :harddisk
end
