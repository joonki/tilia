class Job < ApplicationRecord
  paginates_per 150

  default_scope { order(auftrags_nr: :desc) }
end
