class Invoice < Job
  paginates_per 150

  default_scope { order(datum_auftrag: :desc) }
end
