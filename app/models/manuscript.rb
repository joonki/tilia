class Manuscript < ApplicationRecord
  paginates_per 150

  has_many :manuscripts_harddisks
  has_many :harddisks, through: :manuscripts_harddisks

  default_scope { order(id: :desc) }
end
